//
// EEPROM Loader for KIM Uno
// writes hexadecimal values in EEPROMContents string to Arduino EEPROM
//
// @arduinoenigma 2018
//
// To use this program, upload to KIM Uno:
//
// Tools->Board->Arduino Pro or Pro Mini
// Tools->Processor->Select the actual processor on your board (AtMega328 5V, 16Mhz)
// Tools->Port->Select the port for your KIM
// Sketch->Upload
//
// After the sketch is uploaded:
//
// Tools->Serial Monitor
// Select a speed of 9600 at the bottom right of the Serial Monitor window
// A menu should appear, send 1, 2, or 3 to view or load EEPROM contents
//
// after running this program, reload the KIM Uno program
// memory contents can be viewed by pressing [AD] 0400 [+] [+]...
// to execute programs, press [AD] 0500 [GO]
// where 0500 is the address of the program to execute
//
// EEPROMContents is ready to accept object code from https://www.masswerk.at/6502/assembler.html
// the numbers of characters per line does not matter
//
// TOC
// 0x400 - CLOCK [AD]0024 SEC [AD]0025 MIN [AD]0026 HOUR
// 0x450 - DISPLAY KEYS USING 1F6A ROUTINE
// 0x460 - DEADBEEF AND DISPLAY KEYS WITH 1F1F ROUTINE
// 0x490 - EMPTY
// 0x500 - ENIGMA Z (https://arduinoenigma.blogspot.com/p/enigma-z30-for-kim-uno.html)
// 0x7B0 - EMPTY
//

#include <EEPROM.h>

unsigned int memstart = 0x400; // 1024
unsigned int memend   = 0x7ff; // 2047

const char EEPROMContents[] PROGMEM  = {
  "F8 18 A2 00 A9 01 85 29 A0 00 A5 29 75 24 95 24" // 400..
  "DD 3D 04 F0 06 B4 24 A9 00 85 29 94 24 94 F9 E8" // 410..
  "E0 03 D0 E4 A9 33 85 28 20 1F 1F A9 48 85 27 EA" // 420..
  "EA EA C6 27 D0 F9 C6 28 D0 EE 4C 00 04 60 60 24" // 430..
  "60 EA EA 60 FF FF FF FF FF FF FF FF FF FF FF FF" // 440..
  "20 1F 1F 20 6A 1F C9 1F B0 F6 85 F9 4C 50 04 FF" // 450..
  "20 66 04 4C 60 04 A9 DE 85 FB A9 AD 85 FA 20 79" // 460..
  "04 A9 BE 85 FB A9 EF 85 FA A2 FF A9 00 20 1F 1F" // 470..
  "85 F9 CA D0 F6 60 FF FF FF FF FF FF FF FF FF FF" // 480..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 490..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 4A0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 4B0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 4C0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 4D0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 4E0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 4F0..
  "4C 67 05 00 06 0E 0A 00 01 02 03 01 01 01 01 04" // 500..
  "03 02 01 00 00 1E 00 0A 14 09 06 04 01 08 02 07" // 510..
  "00 03 05 02 05 08 04 01 00 09 07 06 03 04 03 05" // 520..
  "08 01 06 02 00 07 09 02 05 00 07 09 01 08 03 06" // 530..
  "04 07 03 05 08 02 09 01 06 04 00 05 04 00 09 03" // 540..
  "01 08 07 02 06 07 04 06 01 00 02 05 08 03 09 10" // 550..
  "0C 11 0D 14 0E 12 0F D8 A9 00 C5 50 D0 10 A9 12" // 560..
  "85 5E A2 00 BD 03 05 95 4B E8 C6 5E D0 F6 20 B1" // 570..
  "05 20 C0 05 C9 15 B0 F6 C9 0A 90 10 F0 1A C9 0B" // 580..
  "F0 19 C9 13 F0 18 20 DF 05 F0 E3 00 85 5B 20 20" // 590..
  "06 A9 50 85 5D 10 D7 00 4C 7E 05 4C FC 06 4C 6E" // 5A0..
  "05 A9 00 C5 5D F0 08 C6 5D D0 04 85 5B 85 5C 60" // 5B0..
  "A0 00 A2 03 B9 57 00 0A 0A 0A 0A 95 F8 C8 B9 57" // 5C0..
  "00 15 F8 95 F8 C8 CA D0 EB 20 1F 1F 4C 6A 1F A2" // 5D0..
  "00 A0 04 86 5E DD 5F 05 F0 0D DD 60 05 F0 0F E6" // 5E0..
  "5E E8 E8 88 D0 EF 60 A6 5E F6 57 D0 05 00 A6 5E" // 5F0..
  "D6 57 A9 00 20 BB 05 A0 02 84 5F A4 4B B5 57 D9" // 600..
  "C0 06 D0 03 B9 C1 06 C8 C8 95 57 C6 5F D0 EE 60" // 610..
  "A0 01 84 5E A2 03 A9 00 B5 57 EA 48 C9 09 D0 10" // 620..
  "A5 4F C9 00 D0 06 E0 00 F0 06 84 5E E6 5E E6 5E" // 630..
  "18 66 5E 90 07 F6 57 20 07 06 A0 01 68 C9 09 F0" // 640..
  "06 A5 4F C9 00 D0 05 CA E0 FF D0 CA A2 00 86 61" // 650..
  "CA 86 60 A0 07 A2 03 A5 5B 20 AE 06 86 5E 48 8A" // 660..
  "C9 00 F0 03 B5 4F AA 68 18 65 61 7D 15 05 AA BD" // 670..
  "19 05 48 A9 00 A6 5E 20 AE 06 85 5F 68 38 E5 5F" // 680..
  "B0 02 69 0A 85 5C 98 C9 04 D0 08 A9 01 85 60 A9" // 690..
  "28 85 61 8A 18 65 60 AA A5 5C 88 D0 BC 60 38 75" // 6A0..
  "57 C9 0A 90 02 E9 0A 38 F5 53 B0 02 69 0A 18 60" // 6B0..
  "FF 09 0A 00 00 03 04 01 FF 01 02 00 85 5E B5 00" // 6C0..
  "99 00 00 E8 C8 C6 5E D0 F5 60 A2 0F 86 5B 20 C0" // 6D0..
  "05 C9 15 B0 F5 C9 13 D0 07 BA E8 E8 9A 4C 6E 05" // 6E0..
  "C9 0B F0 07 90 E4 48 20 DF 05 68 60 A9 04 85 4B" // 6F0..
  "A2 57 A0 63 20 CC 06 A9 03 A2 50 A0 58 20 CC 06" // 700..
  "A2 00 86 57 86 60 86 61 86 62 E8 86 5C EA EA 20" // 710..
  "DA 06 C9 0B F0 03 D0 E8 00 A9 01 A6 58 95 5F A6" // 720..
  "59 95 5F A6 5A 95 5F A9 00 18 65 60 65 61 65 62" // 730..
  "C9 03 F0 08 A9 0E 20 BB 05 10 D2 00 A2 58 A0 50" // 740..
  "20 CC 06 A9 04 A2 53 A0 57 20 CC 06 A9 00 85 4B" // 750..
  "A2 02 86 5C 20 DA 06 C9 0B F0 03 D0 F3 00 A9 08" // 760..
  "85 4B A9 04 A2 57 A0 53 20 CC 06 A9 04 A2 4C A0" // 770..
  "57 20 CC 06 A2 03 86 5C 20 DA 06 C9 0B F0 07 A5" // 780..
  "5A 85 4F 10 E6 00 A9 04 A2 63 A0 57 20 CC 06 A9" // 790..
  "00 85 4B 20 BB 05 4C 81 05 FF FF FF FF FF FF FF" // 7A0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 7B0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 7C0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 7D0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 7E0..
  "FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF" // 7F0..
  "\0"
};

unsigned int showaddr = 0;
byte showval = 0;
byte showcount = 0;
byte showheader = 0;

void showvalue()
{
  if (showheader == 0)
  {
    if (showaddr < 16)
    {
      Serial.print(F("000"));
    } else if (showaddr < 256)
    {
      Serial.print(F("00"));
    } else
    {
      Serial.print(F("0"));
    }

    Serial.print(showaddr, HEX);
    Serial.print(F(" "));
    showheader = 1;
  }

  if (showval < 16)
  {
    Serial.print(F("0"));
  }
  Serial.print(showval, HEX);
  Serial.print(F(" "));

  showcount++;

  if (showcount > 15)
  {
    Serial.println(F(""));
    showcount = 0;
    if (showheader == 1)
    {
      showheader = 0;
    }
  }
}

void dumpeeprom()
{
  unsigned int addr = 0;
  unsigned int kimaddr = 0x400;
  byte eeval = 0;
  byte valcount = 0;

  Serial.println(F("dump eeprom start"));

  showcount = 0;
  //showheader = 0; //let the caller decide, be sure to initialize it to 0 (header) or 255 (no header)

  do
  {
    eeval = EEPROM.read(addr);

    showaddr = kimaddr;
    showval = eeval;
    showvalue();

    kimaddr++;
    addr++;

  } while (addr < EEPROM.length());

  Serial.println(F("dump eeprom done"));
}

void filleeprom()
{
  byte cx = 0;
  byte cxv = 0;
  bool cxvone = false;
  bool cxvtwo = false;

  unsigned int p = 0;
  unsigned int addr = 0;
  unsigned int kimaddr = 0x400;

  Serial.println(F("fill eeprom start"));

  showcount = 0;
  showheader = 0;

  do
  {
    cx = pgm_read_byte(EEPROMContents + p);

    if (cx == ' ')
    {
      if (cxvone)
      {
        cxvtwo = true;
      }
    }
    else
    {
      cxv = (cxv * 16) + ((cx > '9') ? ((cx & 223) - 'A' + 10) : (cx - '0'));

      if (cxvone)
      {
        cxvtwo = true;
      }
      cxvone = true;
    }

    if (cxvtwo)
    {
      if ((kimaddr >= memstart) && ( kimaddr <= memend))
      {
        EEPROM.write(addr, cxv);

        showaddr = kimaddr;
        showval = cxv;
        showvalue();
      }

      addr++;
      kimaddr++;

      cxv = 0;
      cxvone = false;
      cxvtwo = false;
    }

    p++;

  } while (cx != 0);

  Serial.println(F("fill eeprom done"));
}

void cleareeprom()
{
  unsigned int addr = 0;

  Serial.println(F("clear eeprom start"));

  do
  {
    EEPROM.write(addr, 255);
    addr++;
  } while (addr < EEPROM.length());

  Serial.println(F("clear eeprom done"));
}

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  char cx;

  Serial.println(F("1-show eeprom"));
  Serial.println(F("2-show eeprom without address headers"));
  Serial.println(F("3-write eeprom"));
  Serial.println(F("4-clear eeprom"));
  Serial.print(F("select 1,2 or 3: "));

  do
  {
    do
    {
    } while (Serial.available() == false);
    cx = Serial.read();
  } while ((cx == 13) || (cx == 10));
  Serial.println(cx);
  Serial.println(F(""));

  if (cx == '1')
  {
    showheader = 0;
    dumpeeprom();
  }
  if (cx == '2')
  {
    showheader = 255;
    dumpeeprom();
  }
  if (cx == '3')
  {
    filleeprom();
  }
  if (cx == '4')
  {
    cleareeprom();
  }

  Serial.println(F(""));
}
